// https://jsonplaceholder.typicode.com/todos
let toDoTitle = [];
// Item 3 & 4
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((response) => response.map((items) => toDoTitle.push(items.title)));
console.log(toDoTitle);
// Item 5 & 6
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((response) => console.log(response));
// Item 7
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    completed: false,
    title: "Add new task",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((response) => console.log(response));
// Item 8 & 9
fetch("https://jsonplaceholder.typicode.com/todos/2", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Update a task",
    description: "Wash the Dishes",
    status: "completed",
    dateCompleted: "Jan 1, 2023",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((response) => console.log(response));
// Item 10 & 11
fetch("https://jsonplaceholder.typicode.com/todos/3", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    status: "completed",
    dateUpdated: "Jan 1, 2023",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((response) => console.log(response));
// Item 12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE",
})
  .then((response) => response.json())
  .then((response) => console.log(response));
